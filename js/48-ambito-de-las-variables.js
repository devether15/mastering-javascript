'use strict'

function holaMundo(texto) {
    var hola_mundo = "texto dentro de funcion"
    console.log(texto);
    console.log(numero);//puedo llamarlo porque es una variable global
    console.log(hola_mundo);//puedo llamarla acá porque está definida en el mismo ámbito (variable local)
}

//console.log(hola_mundo);//acá no puedo llamarla porque no está definida en el mismo ámbito (es local de la funcion, y desde acá sería un ambito global)

var numero = 12;
console.log( typeof numero.toString() ); //permite pasar un número a string, con typeof corroboramos el tipo.
var texto = 'Hola mundo soy una variable global';
holaMundo(texto);