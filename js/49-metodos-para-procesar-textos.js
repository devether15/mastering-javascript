'use strict'

//transformacion de cadenas de textos

var numero = 444;
var texto1 = 'Bienvenido al curso de JavaScript de Victor Robles';
var texto2 = 'el curso es bueno';

//transformas número a string
var dato = numero.toString();
console.log(1,dato);
console.log(2,typeof dato);

//convertir minisculas a mayusculas
dato = texto1.toUpperCase();
console.log(3,dato)

//convertir  mayusculas a minisculas
dato = texto1.toLocaleLowerCase();
console.log(4,dato)

// calcular la longitud de un texto 
var nombre = '';
var nombre2 = 'Blabla';
var miArray = ['jose', 'arevalo'];
 
console.log(4,nombre.length);
console.log(5,nombre2.length);
console.log(6,miArray.length); //también sirve para contar arrays

//concatenar textos
var textoTotal = texto1 + ", " + texto2;
console.log(7,textoTotal);

var textoTota2 = texto1.concat(", " + texto2); //se puede lograr lo mismo de la linea 32 con el método concat()
console.log(8,textoTota2);


