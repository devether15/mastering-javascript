'use strict'

//funciones anonimas
//es una funcion que no tiene nombre o firma

//funcion normal
var pelicula = function(nombre) {
    return 'la pelicula es: '+nombre;
}

console.log(pelicula('batman'));

//otro ejemplo
function sumame(num1, num2) {
    var sumar = num1 + num2;
    return sumar;
} 

console.log(sumame(4,5));

//ejemplo con funciones anónimas
function sumame2(num1, num2, sumaYmuestra, sumaPorDos) {
    var sumar = num1 + num2;

    sumaYmuestra(sumar);
    sumaPorDos(sumar);

    return sumar;
}
//la declaración de la función anónima está en el parámetro del llamado de la función normal
sumame2(5,7, function(data){
    console.log('La suma es:', data)
},
function(data) {
    console.log('La suma por dos es:', (data*2))
});
