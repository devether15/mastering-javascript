function sumame2(num1, num2, sumaYmuestra, sumaPorDos) {
    var sumar = num1 + num2;

    sumaYmuestra(sumar);
    sumaPorDos(sumar);

    return sumar;
}

// sumame2(5,7, function(data){ // se reemplaza la palabra function y se pone una flecha delante del parámetro
sumame2(5,7, (data) => {
    console.log('La suma es:', data)
},
data => {
    console.log('La suma por dos es:', (data*2))
});

//cuando la funcion de callback tiene un solo parámetro, se pueden sacar los parentesis como en el caso de la linea 15
